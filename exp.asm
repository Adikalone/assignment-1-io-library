global _start
%include "mylib.inc"

section .data
test: 
	db 'abcded', 0
test2: db "abc1ded", 0

test3:
	db "abc0ded", 0
test4:
	db 20, ''
	
section .text
_start:
;	lea rdi, [test2]
;	call print_string
;	mov rdi, rax
	mov r11, 48
	mov rdi, r11
	call print_char
	call print_newline
	mov rdi, 35
	call print_char
	call print_newline
	
	mov rsi, test3
	mov rdi, test2
	
	call string_equals
	mov rdi, rax
	
	mov rdi, 159
	call print_uint
	call print_newline
	
	mov rdi, 1234567890
	call print_uint
	
	call print_newline

	mov rdi, 1
	call print_uint
	call print_newline
	
	mov rdi, 1234605616436508552
	call print_uint
	call print_newline





	mov rdi, -1234
	call print_int	
	call print_newline
	
	mov rdi, -123456789
	call print_int
	call print_newline	

	mov rdi, -1
	call print_int
	call print_newline	


	
	mov rdi, -234605616436508552
	call print_int
	call print_newline


;	call read_char
;	mov rdi, rax
;	call print_char
;	call print_newline

	mov rdi, test4
	mov rsi, 40
	call read_word
	push rdx
	mov rdi, test4
	call print_string
	call print_newline
	
	pop rdi
	call print_uint
	call print_newline


	mov rdi, test4
	call parse_int
	push rdx
	mov rdi, rax
	call print_int
	call print_newline
;	mov rdi, rax
	
	pop rdi
	call print_uint
	call print_newline





	

	call exit

