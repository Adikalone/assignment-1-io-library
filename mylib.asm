global exit
global string_length
global print_string
global print_char
global print_newline
global string_equals
global print_uint
global print_int
global read_char
global read_word
global parse_uint
global parse_int
	
section .text

exit:
	mov rax, 60
	syscall

string_length:
	mov r13, 0
	.loop:
		cmp byte [rdi + r13], 0
		je .end
		inc r13
		jmp .loop
	.end:
		mov rax, r13
		ret
print_string:
	push rdi
	call string_length
	mov rdx, rax ; В r13 хранится длина строки
	pop rdi
	mov rsi, rdi
	mov rax, 1	
	mov rdi, 1
	syscall
	ret
print_char:
	push rdi	
	mov rsi, rsp
	mov rax, 1 
	mov rdi, 1 
	mov rdx, 1
	syscall
	pop rdi
	ret

print_newline:
	push rdi
	push 0xA
	
	mov rax, 1
	mov rdx, 1
	mov rsi, rsp
	mov rdi, 1
	syscall
	pop rdi
	pop rdi
	ret

print_uint:
	; rdi - хранит наше число
	push rbx
	push rax
	mov rax, rdi ; rax - само наше число, которое мы будем делить

	xor rdx, rdx

	mov rdi, 0xa ; основание системы счисления
	mov rbx, rsp ; rbx - адрес нашей строки (числа)
	sub rbx, 1024
	
	xor r8, r8
	mov [rbx], r8b;	push 0
	sub rbx, 1
	.loop:
		xor rdx, rdx
		div rdi
		mov rcx, rdx
		add rdx, 48 ; превращаем число в символ
		mov [rbx], dl	
		sub rbx, 1
		cmp rax, 0
		jne .loop
	.print:
		add rbx, 1
		mov rdi, rbx
		call print_string
	.end:
		pop rax
		pop rbx
		ret	


print_int:
	; rdi - приходит значение
	push rax
	xor rax, rax
	cmp rax, rdi
	js .plus
	je .plus


	neg rdi
	mov rax, rdi
	mov rdi, 0x2d
	push rax
	call print_char
	pop rax
	mov rdi, rax
	pop rax
	call print_uint
	ret
	.plus:
		call print_uint
		pop rax
	ret
			 



string_equals:
	; rdi - указатель на первую строку
	; rsi - указатель на вторую строку
	push r10 ; будет длинной первой строки
	push r11 ; будет длинной второй строки
	
	call string_length
	mov r10, rax
	
	push rdi
	mov rdi, rsi
	call string_length
	mov r11, rax
	pop rdi
	
	cmp r10, r11
	jne .with0
	
	mov rcx, 0
	push r8
	push r9
	.loop:
		mov r8, [rdi + rcx]
		mov r9, [rsi + rcx]
		cmp r8, r9
		jne .with0
		cmp rcx, r10
		jne .with1
		inc rcx
		jmp .loop


	.end:
		pop r9
		pop r8
		pop r11
		pop r10
		ret
	.with1:
		mov rax, 1
		jmp .end
	.with0:
		mov rax, 0
		jmp .end


read_char:
	push r13
	push rcx
	push r11
	
	mov r13, rsp
	sub r13, 0x80
	mov byte[r13], 0
	

	mov rax, 0 ; num read sys
	mov rdi, 0 ; stdin fd
	mov rsi, r13 ; buf
	mov rdx, 1; size
	
	
	syscall ; read syscal
	cmp rax, 0
	je .end
	
	mov al, byte [r13]	
	
	.end:
		pop r11
		pop rcx
		pop r13
		ret
		

read_word:
	; 0x20 - 
	; 0x9 - skip in start, end in the end
	; 0xA -
	; rdi - адрес начала буффера
	; rsi - длина буфера
	
;save section
	
	mov r10, rdi ; адрес
	mov r11, rsi ; длина
	
	xor rcx, rcx ; счетчик



	.readSymbol:
		push r10
		push r11
		push rcx
		call read_char
		; rax = our char

		pop rcx
		pop r11
		pop r10
	.Space:
		cmp rax, 0x9
		je .SpaceCondition
		cmp rax, 0xA
		je .SpaceCondition
		cmp rax, 0x20
		je .SpaceCondition
	;	cmp rax, 0x0
	;	je .SpaceCondition
		jmp .checkConditions
	.SpaceCondition:	
		cmp rcx, 0
		je .readSymbol
		jmp .end 
	.checkConditions:
		cmp r11, rcx
		je .endwithproblem
		; end of len of the buffer	
	.writeSymbol:
		mov byte [r10 + rcx], al
		inc rcx
		jmp .readSymbol
	.end:
		mov byte [r10 + rcx], 0
		mov rax, r10
		mov rdx, rcx
		ret
	
	.endwithproblem:	
		mov rax, 0
		ret

parse_uint:
	; принимает указатель на строку
	; rdi - указатель на строку
	
	; коды цифр 
	; 0 = 0х30 = 48
	; 9 = 0х39 = 57


	; буду считывать цифру. Если есть ещё одна, домножаем на 10 и прибавляем её, поворить.
	; можно читать и класть нужные символы на стек. Определяем длину нашего числа и возвращаемся
;start position
	
	xor rcx, rcx ; счетчик цифр
	mov r8, rdi  ; указатель на строку
	xor rdx, rdx ; текущая цифра
	
	.getchar:
		mov dl, byte [r8 +rcx]
	.Conditionforend:
		cmp rdx, 0x30
		jb .beforemath
		cmp rdx, 0x39
		ja .beforemath
	.putonsteck:
		sub rdx, 0x30
		push rdx
		inc rcx
		jmp .getchar
	
	.beforemath:
		mov r9, rcx
		xor rcx, rcx
		xor rsi, rsi ; наше будующее число
		mov rax, 1 ; будем наращивать 10 здесь
		xor r10, r10 ; будет цифрой, которую мы снимаем со стека
		
		; r9 - длина нашей цифры
	.math:
		cmp r9, 0
		je .endwithproblem
		; текущее * 10^(ткущая длина) + результат_предыдущего
		inc rcx
		pop r10
		push rax
		mul r10
		xor rdx, rdx
		add rsi, rax
		pop rax 

		cmp r9, rcx
		je .end
		
		; в rax находится степень 10
		mov r10, 10
		mul r10
		xor rdx, rdx
		jmp .math
	.end:
		mov rax, rsi
		mov rdx, r9
		ret
	
	.endwithproblem:
		mov rdx, 0
		mov rax, 0
		ret

		
parse_int:
	; rdi - string pointer
	mov r8, rdi
	.getfirst:
		xor rsi, rsi
		mov sil, byte [r8]
		cmp rsi, 0x2d
		je .sign
	.nosign:
		mov rdi, r8
		call parse_uint
		jmp .end
	.sign:
		inc r8
		mov rdi, r8
		call parse_uint
		cmp rdx, 0
		je .end
		push rdx
		mov r10, -1
		imul r10
		pop rdx
		inc rdx
	.end:
		ret
	
		
string_copy:
	; rdi - string pointer
	; rsi - buffer pointer
	; rdx - buffer length

	mov r8, rdi
	mov r9, rsi
	mov r10, rdx
	
	xor rsi, rsi; 
	xor rcx, rcx
	.readchar:
		mov sil, byte [r8 + rcx]
	.conditionlenbuf:
		cmp rsi, 0
		je .end 
		;
		cmp r10, rcx
		je .endwithproblem
	.writechar:
		mov byte [r9 + rcx], sil
		inc rcx
		jmp .readchar
	.end:
		mov byte [r9 + rcx], 0
		mov rax, rcx
		ret
	.endwithproblem:
		mov rax, 0
		ret





